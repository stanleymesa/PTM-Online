package com.ptm.ppb_project.data;

public class DataKelas {
    String[] kelas = new String[]{"10", "11", "12"};
    String[] namaKelas = new String[]{"A", "B", "C"};
    String[] hari = new String[]{"Senin", "Selasa", "Rabu", "Kamis", "Jumat", "Sabtu", "Minggu"};
    String[] matpel = new String[]{"Matematika", "Biologi", "Kimia", "Fisika"};


    public String[] getKelas() {
        return kelas;
    }

    public String[] getNamaKelas() {
        return namaKelas;
    }

    public String[] getHari() {
        return hari;
    }

    public String[] getMatpel() {
        return matpel;
    }
}
